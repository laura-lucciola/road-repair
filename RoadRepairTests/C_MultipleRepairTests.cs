﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoadRepair;
using RoadRepairInterfaces;

namespace RoadRepairTests
{
    [TestClass]
    public class C_MultipleRepairTests
    {
        [TestMethod]
        public void ThreeRepairs()
        {
            var planner = new Planner();
            var road1 = new Road() { Length = 3, Width = 2, Potholes = 5 };
            var road2 = new Road() { Length = 4, Width = 1.5, Potholes = 3};
            var road3 = new Road() { Length = 6, Width = 2, Potholes = 1};
            var roads = new List<Road>(){ road1, road2, road3 };

            // make the planner calculate the total volume of all the repairs for the list of roads.
            var totalVolume = planner.GetVolume(roads);

            Assert.AreEqual(1.3, totalVolume, SimilarDoubleHandler.Delta);
        }

        [TestMethod]
        public void LimitedMaterial()
        {
            var planner = new Planner();
            var road1 = new Road() { Length = 6, Width = 2, Potholes = 1 };
            var road2 = new Road() { Length = 3, Width = 2, Potholes = 5 };
            var road3 = new Road() { Length = 4, Width = 1.5, Potholes = 3 };            
            var roads = new List<Road>() { road1, road2, road3 };
            var availableMaterial = 1.25;

            // make the planner work out which roads to repair, if there is not enough material available to repair all of them.
            // Try to make the planner select the roads that will fix the highest number of potholes.

            // So for this test, the total volume of the repairs to the selected roads must not exceed the available material.
            // Note that the available material (1.25) is less than the total volume (1.3) needed to fix all 3 roads in the previous test.

            var roadsRepaired = planner.SelectRoadsToRepair(roads, availableMaterial);

            var potholesRepaired = 0;
            foreach (var road in roadsRepaired)
            {
                potholesRepaired += road.Potholes;
            }

            Assert.IsTrue(potholesRepaired >= 8);

            var materialUsed = planner.GetVolume(roadsRepaired);
            Assert.IsTrue(materialUsed <= availableMaterial);
        }
    }
}
