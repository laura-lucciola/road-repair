﻿namespace RoadRepairInterfaces
{
    public interface IRepairType
    {
        public double GetVolume();
    }
}
