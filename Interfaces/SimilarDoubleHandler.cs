﻿using System;

namespace RoadRepairInterfaces
{
    /// <summary>
    /// Custom double type handler
    /// Used to avoid rounding errors when comparing doubles
    /// </summary>
    public static class SimilarDoubleHandler
    {
        public const double Delta = 0.00001;

        /// <summary>
        /// Returns true if the isGreaterEqual param is bigger or sufficiently close to the comparedDouble, by a order of magnitude Delta
        /// </summary>
        /// <param name="isGreaterEqual"></param>
        /// <param name="comparedDouble"></param>
        /// <returns></returns>
        public static bool GreaterEqualThen(double isGreaterEqual, double comparedDouble)
        {
            return isGreaterEqual > comparedDouble || Equals(isGreaterEqual, comparedDouble);
        }

        /// <summary>
        /// Returns true if the isEqual param is sufficiently close to the comparedDouble, by a order of magnitude Delta
        /// </summary>
        /// <param name="isEqual"></param>
        /// <param name="comparedDouble"></param>
        /// <returns></returns>
        private static bool Equals(double isEqual, double comparedDouble)
        {
            return Math.Abs(isEqual - comparedDouble) < Delta;
        }
    }
}
