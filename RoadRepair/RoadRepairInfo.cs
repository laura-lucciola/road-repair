﻿using RoadRepairInterfaces;
using System.Collections.Generic;
using System.Linq;

namespace RoadRepair
{
    public class RoadRepairInfo
    {
        public List<RoadRepairItem> RoadRepairList { get; set; }
        public double TotalMaterialToRepair { get; private set; }

        public RoadRepairInfo(List<Road> roads)
        {
            RoadRepairList = new List<RoadRepairItem>();
            foreach (var road in roads)
            {
                AddNewItem(road);
            }
        }

        public void AddNewItem(Road road)
        {
            var roadRepairInfo = new RoadRepairItem(road);
            RoadRepairList.Add(roadRepairInfo);
            TotalMaterialToRepair += roadRepairInfo.MaterialToRepair;
        }

        public bool GetMaterialAvailability(double availableMaterial)
        {
            return SimilarDoubleHandler.GreaterEqualThen(availableMaterial, TotalMaterialToRepair);
        }


        public IEnumerable<RoadRepairItem> OrderByLeastPotholes()
        {
            return RoadRepairList.OrderBy(road => road.Road.Potholes);
        }


        /// <summary>
        /// This method will iteratively remove the roads with least potholes from the list
        /// Until the current cost of materials is lower then the available materials
        /// It does not handle (yet) the cases that some roads with few potholes have a low cost, and could be added back to the list
        /// </summary>
        /// <param name="availableMaterial"></param>
        /// <returns>List of RoadRepairItem, order by least amount of potholes first</returns>
        public List<RoadRepairItem> GetReparableRoads(double availableMaterial)
        {
            var totalNeededMaterial = TotalMaterialToRepair;
            var orderedList = OrderByLeastPotholes().ToList();

            for (int i = 0; i < orderedList.Count; i++)
            {
                var currentNeededMaterial = totalNeededMaterial - orderedList[i].MaterialToRepair;
                totalNeededMaterial -= orderedList[i].MaterialToRepair;
                orderedList.RemoveAt(i);

                if (SimilarDoubleHandler.GreaterEqualThen(currentNeededMaterial, availableMaterial))
                {
                    continue;
                }
                else
                {
                    return orderedList;
                }
            }           

            return orderedList;
        }
    }
}
