﻿using RoadRepairInterfaces;
using System.Collections.Generic;
using System.Linq;

namespace RoadRepair
{
    public class Planner
    {
        /// <summary>
        /// The total number of hours needed to complete the work.
        /// </summary>
        public double HoursOfWork { get; set; }

        /// <summary>
        /// The number of people available to do the work
        /// </summary>
        public int Workers { get; set; }

        /// <summary>
        /// The time to complete the work, using all the workers.
        /// </summary>
        /// <returns>The number of hours to complete the work.</returns>
        public double GetTime()
        {
            var time = HoursOfWork / Workers;

            return time;
        }

        /// <summary>
        /// Return the correct type of repair (either a patch or a resurface) depending on
        /// the density of potholes.
        /// </summary>
        /// <param name="road">A road needing repair</param>
        /// <returns>Either a PatchingRepair or a Resurfacing</returns>
        public IRepairType SelectRepairType(Road road)
        {
            return RoadRepair.SelectRepairType(road);
        }

        /// <summary>
        /// Calculate the total volume of all the repairs for a list of roads that need repairs.
        /// </summary>
        /// <param name="roads">A list of roads needing repairs</param>
        /// <returns>The total volume of all the repairs</returns>
        public double GetVolume(List<Road> roads)
        {
            var totalVolume = new RoadRepairInfo(roads).TotalMaterialToRepair;
            return totalVolume;
        }

        /// <summary>
        /// Given a list of roads and a quantity of available material
        /// The planner will do a simple calculation to display which roads should be repaired in order to fix the highest number of potholes.
        /// </summary>
        /// <param name="roads">List of roads that need repairs</param>
        /// <param name="availableMaterial">Quantity of resource available to fix the roads (e.g.: volume of asphalt)</param>
        /// <returns></returns>
        public List<Road> SelectRoadsToRepair(List<Road> roads, double availableMaterial)
        {
            var roadRepairInfo = new RoadRepairInfo(roads);
            var sufficientMaterialForRepairs = roadRepairInfo.GetMaterialAvailability(availableMaterial);

            if (sufficientMaterialForRepairs)
            {
                return roads;
            }

            var reparableRoads = roadRepairInfo.GetReparableRoads(availableMaterial);

            return ToRoadList(reparableRoads);
        }


        private static List<Road> ToRoadList(List<RoadRepairItem> roadRepairItems)
        {
            return roadRepairItems.Select(item => item.Road).ToList();
        }
    }
}
